package activitystreamer.server;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import activitystreamer.util.Settings;


public class Connection extends Thread {
    private static final Logger log = LogManager.getLogger();

    private DataInputStream in;
    private DataOutputStream out;
    private BufferedReader inreader;
    private BufferedWriter outwriter;
    private PrintWriter printwriter;
    private boolean open = false;
    private Socket socket;
    private boolean term = false;
    private boolean isServer;
    private boolean authenticated;
    private Settings localSetting;

    public static final int maxAttempts = 10;

    public Connection(Socket socket) throws IOException {
        in = new DataInputStream(socket.getInputStream());
        out = new DataOutputStream(socket.getOutputStream());
        inreader = new BufferedReader(new InputStreamReader(in));
        outwriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));
        printwriter = new PrintWriter(out, true);
        this.socket = socket;
        open = true;
        start();
    }

    /*
     * returns true if the message was written, otherwise false
     */
    public boolean writeMsg(String msg) {
        if (open) {
            try {
                outwriter.write(msg + "\n");
                outwriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
        log.fatal("ERROR WRITING TO STREAM");
        return false;
    }

    public void closeCon() {
        if (open) {
            log.info("closing connection " + Settings.socketAddress(socket));
            try {
                term = true;
                inreader.close();
                out.close();
                //TODO Remove from connections array
            } catch (IOException e) {
                // already closed?
                log.error("received exception closing the connection " + Settings.socketAddress(socket) + ": " + e);
            }
        }
    }

    /**
     * Separate listening thread for a connection
     */
    public void run() {

        String data = null;
        int attempts = 0;

        while (attempts < maxAttempts) {
            try {
                while (!term && (data = inreader.readLine()) != null) {
                    open = true;
                    log.debug("incoming msg: " + data);
                    //System.out.println(Control.getInstance());
                    term = Control.getInstance().process(this, data);
                }
                log.debug("[CONNECTION] connection closed to " + Settings.socketAddress(socket));
                Control.getInstance().connectionClosed(this);
                in.close();
            } catch (SocketException | SocketTimeoutException e) {
                open = false;
                ServerInfo badServer = Control.getInstance().getServer(this);
                log.error("[CONNECTION] lost to : " + badServer.getID());
            } catch (IOException e) {
                open = false;
                //e.printStackTrace();
                ServerInfo badServer = Control.getInstance().getServer(this);
                log.error("[CONNECTION] lost to : " + badServer.getID());
            }
            attempts++;
            log.fatal("LOST CONNECTION..... Attempt " + attempts + " to read from socket");

            if (reconnect()) {

                ServerInfo badServer = Control.getInstance().getServer(this);
                return;
            }
            sleep();
        }
        Control.getInstance().removeConnections(this);
        closeCon();
        //TODO create new connection thread?
        open = false;
    }

    public boolean reconnect() {
        ServerInfo server = Control.getInstance().getServer(this);
        Socket sock;
        if (this.isServer) {
            try {
                SocketAddress sockaddr = new InetSocketAddress(server.getHostname(), server.getPort());
                sock = new Socket();
                sock.connect(sockaddr, 2000);

                Control.getInstance().resetConnection(this, server, sock);
                log.info("[CONNECTION] : Established reconnect to " + server.getID());
                return true;
            } catch (ConnectException e) {
                log.info("[CONNECTION] : Failed to reconnect to server " + server.getID());
            } catch (IOException e) {

            }
        }
        return false;
    }

    public void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void setServer(boolean isServer) {
        this.isServer = isServer;
    }

    public Socket getSocket() {
        return socket;
    }

    public boolean isOpen() {
        return open;
    }

    public boolean isServer() {
        return isServer;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticate(boolean status) {
        this.authenticated = status;
    }
}
