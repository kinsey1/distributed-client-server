package activitystreamer.server;


import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class to maintain the information about a connected server so that
 * it can be updated during server announces.
 */
public class ServerInfo {

    private String hostname;
    private Integer port;
    private Integer load;
    private String ID;
    private boolean valid = false;
    private ArrayList<String> connectedClients = new ArrayList<String>();
    private HashMap<String, Integer> messageDB = new HashMap<String, Integer>();

    public ServerInfo(String hostname, Integer port, String ID) {
        this.hostname = hostname;
        this.port = port;
        this.ID = ID;
        this.load = 0;

    }

    public ServerInfo(String hostname, Integer port) {
        this.hostname = hostname;
        this.port = port;
        this.load = 0;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public ServerInfo() {
        this.load = 0;

    }

    public String getHostname() {
        return hostname;
    }

    public Integer getPort() {
        return port;
    }

    public void setLoad(Integer load) {
        this.load = load;
    }

    public String getID() {
        return this.ID;
    }

    public boolean isValid() {
        return this.valid;
    }

    public Integer getLoad() {
        return load;
    }

    public void setSecret(String secret) {
        this.ID = ID;
    }

    public String toString() {
        return this.hostname + " " + this.port + " " + this.load;
    }

    public void setConnectedClient(String client) {
        if (!this.connectedClients.contains(client)) {
            this.connectedClients.add(client);
        }
    }

    public void printConnectedClients() {
        //System.out.println("[LOG] My clients are:");

        for (String client : connectedClients) {
            System.out.println(client);
        }
    }

    public ArrayList<String> getConnectedClients() {
        return this.connectedClients;
    }

    public HashMap<String, Integer> getMessageDB() {
        return messageDB;
    }

    public void removeFromStorage(String username) {
        if (this.connectedClients.contains(username)) {
            this.connectedClients.remove(username);
            if (this.messageDB.containsKey(username)) {
                this.messageDB.remove(username);
            }
        }
    }
}
