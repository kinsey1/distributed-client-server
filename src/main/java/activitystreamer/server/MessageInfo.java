package activitystreamer.server;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MessageInfo {

    private JSONObject snapshot;
    private JSONObject serverId;
    private JSONObject clientUsername;
    private JSONObject activity;

    public MessageInfo() {
        this.snapshot = new JSONObject();
        this.serverId = new JSONObject();
        this.clientUsername = new JSONObject();
        this.activity = new JSONObject();

    }

    public MessageInfo(HashMap<String, ArrayList<String>> snap, String server, String client,
                       JSONObject message) {

        this.snapshot = new JSONObject();
        this.serverId = new JSONObject();
        this.clientUsername = new JSONObject();
        this.activity = new JSONObject();

        JSONArray servCli = new JSONArray();
        servCli = getSnapshot(snap);
        this.snapshot.put("serverClients", servCli);
        this.serverId.put("sendingServerId", server);
        this.clientUsername.put("sendingClientUsername", client);
        this.activity.put("activity", message);
    }

    public MessageInfo(JSONArray snap, String server, String client, JSONObject msg) {
        this.snapshot = new JSONObject();
        this.serverId = new JSONObject();
        this.clientUsername = new JSONObject();
        this.activity = new JSONObject();

        this.snapshot.put("serverClients", snap);
        this.serverId.put("sendingServerId", server);
        this.clientUsername.put("sendingClientUsername", client);
        this.activity.put("activity", msg);
    }

    private JSONArray getSnapshot(HashMap<String, ArrayList<String>> snap) {

        JSONArray servCli = new JSONArray();
        for (String ser : snap.keySet()) {
            JSONObject s = new JSONObject();
            ArrayList<String> cli = snap.get(ser);
            JSONArray cliArr = new JSONArray();
            for (String username : cli) {
                cliArr.add(username);
            }
            s.put("SID", ser);
            s.put("Clients", cliArr);
            servCli.add(s);
        }
        return servCli;

    }

    public JSONObject getActivity() {
        return activity;
    }

    public JSONObject getClientUsername() {
        return clientUsername;
    }

    public JSONObject getServerId() {
        return serverId;
    }

    public JSONObject getSnapshot() {
        return snapshot;
    }

}
