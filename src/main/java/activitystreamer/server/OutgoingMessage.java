package activitystreamer.server;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class to generate JSON outgoing messages.
 * Has multiple overloaded constructors to create
 * outgoing messages based on the number of input strings
 */
public class OutgoingMessage {

    public JSONObject reply;

    /**
     * Used for most messages
     *
     * @param command
     * @param message
     */
    public OutgoingMessage(String command, String message) {
        this.reply = new JSONObject();

        switch (command) {
            case "AUTHENTICATE":
                reply.put("command", command);
                reply.put("secret", message);
                break;
            case "ACTIVITY_BROADCAST":
                reply.put("command", command);
                reply.put("activity", message);

                break;
            case "AUTHENTICATION_FAIL":
                reply.put("command", command);
                reply.put("info", "The supplied secret was incorrect :" + message);
                break;
            case "REGISTER_SUCCESS":
                reply.put("command", command);
                reply.put("info", "You registered successfully");
                break;
            case "REGISTER_FAILED":
                reply.put("command", command);
                reply.put("info", "Username already exists");
                break;
            default:
                reply.put("command", command);
                reply.put("info", message);
                break;

        }

    }

    public OutgoingMessage(String command, JSONObject activity) {
        this.reply = new JSONObject();
        reply.put("command", command);
        reply.put("activity", activity);
    }

    public OutgoingMessage(String command, Integer messageNumber, MessageInfo message) {
        this.reply = new JSONObject();
        //switch (command){
        //    case "ACTIVITY_BROADCAST":
        reply.put("command", command);
        reply.put("activity", message.getActivity().get("activity"));
        reply.put("snapshot", message.getSnapshot().get("serverClients"));
        reply.put("messageNumber", messageNumber);
        reply.put("clientUsername", message.getClientUsername().get("sendingClientUsername"));
        reply.put("serverId", message.getServerId().get("sendingServerId"));
        //        break;
        /*    case "CORRECT_MESSAGE_ORDER":
                reply.put("command",command);
                reply.put("activity",message.getActivity().get("activity"));
                reply.put("messageNumber", messageNumber);
                reply.put("clientUsername", message.getClientUsername().get("sendingClientUsername"));
                reply.put("serverId",message.getServerId().get("sendingServerId"));
        }*/

    }

    /**
     * Used for REDIRECT,
     * For all lock related commands the keys are the same
     * LOCK_REQUEST,
     * LOCK_DENIED,
     * LOCK_ALLOWED,
     * AUTHENTICATION_FAIL
     *
     * @param command
     * @param message
     * @param info
     */
    public OutgoingMessage(String command, String message, String info) {
        this.reply = new JSONObject();

        switch (command) {

            case "REDIRECT":
                reply.put("command", command);
                reply.put("hostname", message);
                reply.put("port", info);
                break;
            case "LOGIN":
            case "REGISTER":
            case "LOCK_REQUEST":
            case "LOCK_ALLOWED":
            case "LOCK_DENIED":
                reply.put("command", command);
                reply.put("username", message);
                reply.put("secret", info);
                break;
            case "DISCONNECT_CLIENT":
                reply.put("command", command);
                reply.put("serverId", message);
                reply.put("clientUsername", info);

        }
    }

    /**
     * Used for others
     *
     * @param command
     * @param secret
     * @param load
     * @param hostname
     * @param port
     */
    public OutgoingMessage(String command, String secret, String load, String hostname, String port, ArrayList<Client> connectedClients,
                           JSONArray servClie) {
        reply = new JSONObject();
        JSONArray array = new JSONArray();
        reply.put("command", command);
        reply.put("id", secret);
        reply.put("load", load);
        reply.put("hostname", hostname);
        reply.put("port", port);

        for (Client client : connectedClients) {
            array.add(client.getUsername());
        }
        reply.put("clients", array);
        reply.put("remoteServerDB", servClie);

    }

    public OutgoingMessage(String command, String secret, String ID, String localhost, int port) {
        this.reply = new JSONObject();
        reply.put("command", command);
        reply.put("id", ID);
        reply.put("secret", secret);
        reply.put("lh", localhost);
        reply.put("port", port);
    }

    public OutgoingMessage(String command, String username, String secret, JSONObject activity) {
        reply = new JSONObject();
        switch (command) {

            case "ACTIVITY_MESSAGE":
                reply.put("command", command);
                reply.put("username", username);
                reply.put("secret", secret);
                reply.put("activity", activity);
                break;
        }

    }

    /**
     * Creates an outgoing message with all the clients
     * and client connections
     *
     * @param serverConnections
     * @param clients
     */
    public OutgoingMessage(HashMap<Connection, ServerInfo> serverConnections, ArrayList<Client> clients, String id) {
        reply = new JSONObject();
        JSONArray jsonClients = new JSONArray();
        JSONArray jsonServers = new JSONArray();

        reply.put("command", "AUTHENTICATE_INFO");
        for (ServerInfo server : serverConnections.values()) {
            JSONObject serverJson = new JSONObject();
            serverJson.put("rh", server.getHostname());
            serverJson.put("rp", server.getPort());
            serverJson.put("id", server.getID());
            jsonServers.add(serverJson);

        }
        for (Client client : clients) {
            JSONObject clientJson = new JSONObject();
            clientJson.put("username", client.getUsername());
            clientJson.put("secret", client.getSecret());

        }

        reply.put("servers", jsonServers);
        reply.put("clients", jsonClients);
        reply.put("id", id);

    }


    public String getJsonReply() {
        return this.reply.toJSONString();
    }

    public JSONObject getReply() {
        return reply;
    }
}
