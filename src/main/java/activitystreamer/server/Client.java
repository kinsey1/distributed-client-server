package activitystreamer.server;

import com.google.gson.JsonObject;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Client class to hold simple client information
 */
public class Client {
    private String username;
    private String secret;
    private HashMap<Integer, MessageInfo> messageDB;
    private Integer messageNumber;

    public Client(String username, String secret) {
        this.username = username;
        this.secret = secret;
        this.messageNumber = 0;
        this.messageDB = new HashMap<Integer, MessageInfo>();
    }

    public String getUsername() {
        return username;
    }

    public String getSecret() {
        return secret;
    }

    public Integer getMessageNumber() {
        return messageNumber;
    }

    public void addToMessageDB(MessageInfo msg) {
        this.messageDB.put(++this.messageNumber, msg);
    }

    public MessageInfo getFromMessageDB(Integer messageNumber) {
        MessageInfo msg = new MessageInfo();
        if (this.messageDB.containsKey(messageNumber + 1)) {
            msg = this.messageDB.get(messageNumber + 1);
            return msg;
        } else
            return null;
    }
}
